# YOLO-mask

Real time detection of people wearing or not wearing masks from a video camera.

### Requirements
- Python3.6+
- virtualenv (`pip install virtualenv`)

### Installation
- `virtualenv env`
- `source env/bin/activate` (Linux)
- `env\\Scripts\\activate.bat` (Windows)
- `pip install -r requirements.txt`

### Execution
- `python detector.py`